# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Talk Out Forum
* Key functions (add/delete)
    1. 三個版面
    2. 可上傳文章
    3. Log in
    4. Log out
    5. Register
    
* Other functions (add/delete)
    1. Sign in with google
    2. User Page
    3. animation

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|Firebase Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|

## Website Detail Description

# 作品網址：https://forummmm-6bc9d.firebaseapp.com

# Components Description : 
1. 主頁: 可瀏覽所有文章，不分類別
2. Sign In : 登入、用google登入、創新帳號
3. Register : 創建新帳號，會進行密碼的確認
4. 三版：Gossip,Sports,Hate，可版上留言，文章分門別類
5. Log out：登出

# Other Functions Description(1~10%) : 
1. Animation : 標題動畫
2. User Pate : 點進去標題旁的信箱帳號，即可看到使用者頁面
3. Sign in with google : 登入時可選擇用google帳號登入

## Security Report (Optional)
